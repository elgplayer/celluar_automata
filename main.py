# -*- coding: utf-8 -*-
"""
Created on Mon Nov  12 18:39:20 2020

@author: Carl
"""

from src import io_helper
from src import game_logic


# Nice website
# https://copy.sh/life/


def main():
    '''
    Main function - Celluar automata
    '''

    # Some more variables
    print_board = True
    generation = 0
    nr_of_steps = 0
    interupted = False
    verbose = False
    print_each_generation = False


    # Setup the board and print it
    board, living_cells, matrix_dim = io_helper.user_setup()
    io_helper.print_board(board, matrix_dim)

    while interupted is False:

        # Ask for number of steps to simulate
        nr_of_steps = io_helper.ask_nr_of_steps()

        # Simulate
        living_cells, generation = game_logic.simulate(board,
                                                       living_cells,
                                                       matrix_dim,
                                                       nr_of_steps=nr_of_steps,
                                                       generation=generation,
                                                       print_board=print_board,
                                                       verbose=verbose,
                                                       print_each_generation=print_each_generation)

        # Probe if the program should contiue to simulate
        interupted = io_helper.ask_should_continue()


main()
