# -*- coding: utf-8 -*-
"""
Created on Mon Nov  12 18:39:40 2020

@author: Carl
"""

import os
import sys

from src import game_logic


def user_setup():
    '''
    Asks the user for information to setup the simulation. What type of board
    and how large the board should be

    Return
    ----------
    board : list
        The board as a nested list

    living_cells : dict
        Dictionary with all cells that are alive

    matrix_dim : list
        Dimensions of the matrix [X, Y]
    '''

    file_path_valid = False
    valid_file = False
    valid_x = False
    valid_y = False

    print("Welcome to celluar automata!")
    
    # Loop until the input is valid
    while file_path_valid == False:

        try:

            # Read coordinate file
            if valid_file == False:
                file_path = input("What file do you want to load? ")
                file_data, x_cord_max, y_cord_max = read_cordinate_file(file_path)
                valid_file = True

            # Read board size (X)
            if valid_x == False:
                matrix_dim_x = int(input(f"How wide should the board be? (X >= {x_cord_max+1}))? "))
                if matrix_dim_x < 2:
                    raise ValueError("Needs to be >= 2")
                if matrix_dim_x <= x_cord_max:
                    raise ValueError(f"Needs to be >= {x_cord_max+1}")
                valid_x = True

            # Read board size (Y)
            if valid_y == False:
                matrix_dim_y = int(input(f"How high should the board be? (Y >= {y_cord_max+1})? "))
                if matrix_dim_y < 2:
                    raise ValueError("Needs to be >= 2")
                if matrix_dim_y <= y_cord_max:
                    raise ValueError(f"Needs to be >= {y_cord_max+1}")
                valid_y = True

            # If all is valid, then stop the infinite loop
            if valid_file == True and valid_x == True and valid_y == True:
                file_path_valid = True

        # User abortting
        except KeyboardInterrupt:
            print("Aborting...")
            sys.exit(0)
        
        # Random error
        except Exception as e:
            print(f"Error: {e}")

    # Init the board
    matrix_dim = [matrix_dim_x, matrix_dim_y]
    board, living_cells, matrix_dim = matrix_parsing(file_data, matrix_dim)

    # Init print output
    print(f"File loaded: {file_path} | Living cells: {len(living_cells)} | Matrix dimensions: {matrix_dim}")

    return board, living_cells, matrix_dim


def ask_should_continue():
    '''
    Asks if the user want to contiue simulating the board
    Asks until the input is valid.

    Return
    ----------
    interupted : Boolean
        Stop or contiue
    '''    

    interupted = False
    valid_input = False
    while valid_input == False:
        should_contiue = input("Do you want to continue [y/n]? ")
        if should_contiue == "y" or should_contiue == "n":
            valid_input = True
        else:
            print("Valid generations are an integer >= 1")

    if should_contiue == "n":
        interupted = True
        print("Exiting...")
        
    return interupted


def ask_nr_of_steps():
    '''
    Asks for number of steps to simulate. Asks until the input is valid.

    Return
    ----------
    interupted : Boolean
        Stop or contiue
    '''    

    valid_input = False
    while valid_input == False:
        try:
            nr_of_steps = int(input("How many generations would you like to simulate? "))
            if nr_of_steps < 1:
                raise ValueError("Error! Invalid number of generations!")
            valid_input = True
        except ValueError:
            print("Number of steps needs to be >= 1")

    return nr_of_steps


def read_cordinate_file(file_path):
    '''
    Reads the coordinate file to iniate a given celluar automata simulation

    Input
    ----------
    file_path : str
        Where the coordinate file is stored on the computer

    matrix_dim : list
        Dimensions of the matrix [X, Y]

    Return
    ----------
    data : list
        The coordiante file as list with a string for each line
    '''

    x_cord_max = 0
    y_cord_max = 0

    if not os.path.exists(file_path):
        raise OSError('File path does not exist!')

    # Open the file
    with open(file_path, 'r', encoding='utf-8') as f:
        # Read the file line by line
        data = f.readlines()
        # Check that the file actually has some data
        if len(data) > 0:

            for line in data:
                parsed_cord = line.strip().split(" ")
                x_cord = int(parsed_cord[0])
                y_cord = int(parsed_cord[1])

                if x_cord >= x_cord_max:
                    x_cord_max = x_cord

                if y_cord >= y_cord_max:
                    y_cord_max = y_cord


            return data, x_cord_max, y_cord_max


def matrix_parsing(data, matrix_dim):
    '''
    Setups the board by initating the board given a size and 
    sets all the cells that were read from the coordinate file as alive.

    Input
    ----------
    data : list
        List with X- and Y-coordinates as a string in the format ["X Y"]

    matrix_dim : list
        Dimensions of the matrix

    Return
    ----------
    board : list
        The board as a nested list

    living_cells : dict
        Dictionary with all cells that are alive

    matrix_dim : list
        Dimensions of the matrix [X, Y]
    '''

    if matrix_dim[0] < 2 or matrix_dim[1] < 2:
        raise ValueError("Invalid matrix dimensions! Needs to be >= 2")

    # Init the board
    board = []
    living_cells = {}

    # Iterate over the matrix to initilize the board
    for x in range(matrix_dim[0]):
        y_list = []
        for y in range(matrix_dim[1]):
            cell = game_logic.Cell(x, y)
            y_list.append(cell)
        board.append(y_list)
        
    # Insert a list with all neigbor cells with a class pointer
    for x in range(matrix_dim[0]):
        for y in range(matrix_dim[1]):
            cell = board[x][y]
            cell_neighbors = game_logic.get_neighbors_cordinates(x, y, matrix_dim)
            
            neigbor_list = []
            for neigbor in cell_neighbors:
                neigbor_cell = board[neigbor[0]][neigbor[1]]
                neigbor_list.append(neigbor_cell)
            cell.set_neighbors_list(neigbor_list)

    # Go through all coordinates from the given file
    # And add them as cells in the board
    for cord in data:

        parsed_cord = cord.strip().split(" ")
        x_cord = int(parsed_cord[0])
        y_cord = int(parsed_cord[1])

    # Check for valid X- and Y-Cordinates
    # We assume that the user is aware of 0-indexing
        if x_cord >= matrix_dim[0] or x_cord < 0:
            raise ValueError(f"X-cordinate: {x_cord} is outside the board ({matrix_dim})")

        if y_cord >= matrix_dim[1] or y_cord < 0:
            raise ValueError(f"Y-cordinate: {y_cord} is outside the board ({matrix_dim})")
        
        # Insert the cell at the board
        cell = board[x_cord][y_cord]
        cell.set_living_status(True)

        # Add the cell to the living cell dictionary
        living_cell_key = f'{x_cord}_{y_cord}'
        living_cells[living_cell_key] = cell

    return board, living_cells, matrix_dim


def print_board(board, matrix_dim):
    '''
    Prints out the board

    Input
    ----------
    board : list
        The board as a nested list

    matrix_dim : list
        Dimensions of the board (X, Y)
    '''
    board_str = ""

    # Go through all column
    for y in range(matrix_dim[1]):
        board_row = ""
        # Go through the row
        for x in range(matrix_dim[0]):
            cell = board[x][y]
            board_row += cell.__str__()

        board_row += "\n"
        board_str += board_row
 
    # Print the board
    print(board_str)
