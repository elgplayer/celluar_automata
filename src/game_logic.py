# -*- coding: utf-8 -*-
"""
Created on Mon Nov  12 18:39:30 2020

@author: Carl
"""


from src import io_helper


class Cell:

    def __init__(self, x, y, living=False):
        '''
        Iniate the Cell class given a coordinate and if it is alive or not

        Input
        ----------
        x : int
            X-coordinate

        y : int
            Y-coordinate

        living : Boolean [default=False]
            Is the cell alive?
        '''
        self.__x = x
        self.__y = y
        self.__living = living
        self.__living_neighbors = 0
        self.__neighbors_list = []

    def set_living_status(self, living):
        '''
        Sets the cell as alive

        Input
        ----------
        living : Boolean
            Is the cell alive or not?
        '''
        self.__living = living

    def set_living_neighbors(self, living_neighbors):
        '''
        Sets the number of neighbors that are alive

        Input
        ----------
        living_neighbors : int
            Number of living neighbors
        '''
        self.__living_neighbors = living_neighbors

    def set_neighbors_list(self, neighbors_list):
        '''
        Sets the list of neighbors to the cell

        Input
        ----------
        neighbors_list : list
            List with pointers to the cells neighbors
        '''
        self.__neighbors_list = neighbors_list

    def get_living_status(self):
        '''Gets if the cell is alive or dead'''
        return self.__living

    def get_living_neighbors(self):
        '''Gets the number of living neighbors'''
        return self.__living_neighbors

    def get_neighbors_list(self):
        '''Gets the list of to all neighbor cells'''
        return self.__neighbors_list

    def get_coordinates(self):
        '''Gets coordinate to the cell as a list [X, Y]'''
        return [self.__x, self.__y]

    def get_coordinates_str(self):
        '''Gets coordinate to the cell as a string: X_Y'''
        return f"{self.__x}_{self.__y}"

    def __str__(self):
        '''Graphical reprensentation of the cell'''
        if self.__living == False:
            return "-"
        else:
            return "*"


def get_neighbors_cordinates(x, y, matrix_dim):
    '''
    Gets the coordinates to all neighbors for a cell

    Input
    ----------
    x : int
        X-cordinate

    y : int
        Y-cordinate

    matrix_dim : list
        Dimensions of the matrix [X, Y]

    Return
    ----------
    neighbors_list : list
        List with lists that contains all coordinates
        to the given cells neighbors
    '''
    # The index notation to a cells neighbors
    # is according to this schematic (* = the cell)
    # 1 2 3
    # 4 * 5
    # 6 7 8
    x_left = x - 1
    x_right = x + 1
    y_upper = y - 1
    y_lower = y + 1

    if x_right == matrix_dim[0]:
        x_right = 0

    if x_left == -1:
        x_left = matrix_dim[0] - 1

    if y_upper == -1:
        y_upper = matrix_dim[1] - 1

    if y_lower == matrix_dim[1]:
        y_lower = 0

    neighbors_list = [
        [x_left, y_upper],  # 1
        [x, y_upper],       # 2
        [x_right, y_upper], # 3
        [x_left, y],        # 4
        [x_right, y],       # 5
        [x_left, y_lower],  # 6
        [x, y_lower],       # 7
        [x_right, y_lower], # 8
    ]

    return neighbors_list


def check_cell(cell, cell_queue, check_neighbors=True):
    '''
    Check how many neighbors that are alive

    Input
    ----------
    cell : Cell.object
        A cell object

    cell_queue : list
        List with all cells that we need to process

    check_neighbors : Boolean [default=True]
        Should we check this cells neighbor. This is used to prevent a
        infinite recusrive call from neighbors neighbors cell
        and their neighbors...
    '''
    neighbors_list = cell.get_neighbors_list()
    nr_of_living_neighbors = 0

    for neighbor in neighbors_list:

        neighbor_living = neighbor.get_living_status()

        if neighbor_living == True:
            nr_of_living_neighbors += 1

        # Prevent recursive neighbor call
        if check_neighbors == True:
            check_cell(neighbor, cell_queue, False)

    # Update the number of living neighbors for the cell
    cell.set_living_neighbors(nr_of_living_neighbors)
    # Add the cell to the cell_queue
    cell_queue.append(cell)


def decicde_fate(cell, living_cells, verbose=False):
    '''
    Decides if a cell will be alive or not depending on the amount of neighbors
    it has

    Input
    ----------
    cell : Cell.object
        A cell object

    living_cells : dict
        Dictionary with all cells that are alive

    verbose : Boolean [default=False]
        Should we print stuff
    '''

    alive = cell.get_living_status()
    neighbors = cell.get_living_neighbors()
    new_cell_state = False

    if verbose:
        coordinates = cell.get_coordinates()

    # Living cell
    if alive == True:

        # Death due to over population
        if neighbors > 3:
            new_cell_state = False
            if verbose:
                print(f"Killing! 1 | Cordinates: {coordinates}")

        # Death due to lonliness
        elif neighbors < 2:
            new_cell_state = False
            if verbose:
                print(f"Killing! 2 | Cordinates: {coordinates}")

        # It lives to another day
        else:
            new_cell_state = True

    # A new cell is born
    elif neighbors == 3:
        if verbose:
            print(f"Born! | Cordinates: {coordinates}")
        new_cell_state = True

    # Add thee cell to the living_cell dictionary
    if new_cell_state == True:
        living_cells[cell.get_coordinates_str()] = cell

    # Set the cell fate
    cell.set_living_status(new_cell_state)


def simulate(board, living_cells, matrix_dim, nr_of_steps=1, generation=0,
             print_board=True, verbose=False, print_each_generation=False):
    '''
    Simulates the board with a number of given timesteps

    Input
    ----------
    board : list
        The board as a nested list

    living_cells : dict
        Dictionary with all cells that are alive

    matrix_dim : list
        Dimensions of the matrix [X, Y]

    nr_of_steps : int [default=1]
        Number of generations to simulate

    print_board : Boolean [default=True]
        Should the board be printed at the end of the simulation

    verbose : Boolean [default=False]
        Print out detailed stats about number of living cells and number of cell
        in the processing cell queue

    print_each_generation : Boolean [default=False]
        Print out the board for each generation

    Return
    ----------
    living_cells : dict
        Dictionary with all cells that are alive

    cell_queue : list
        List with all cells that we need to process

    generation : int
        What generation is the board currently simulating
    '''

    cell_queue = []

    # Iterate over the nr of given timesteps
    for i in range(nr_of_steps):

        if verbose:
            print(f"Generation: {i+2} | Living cells: {len(living_cells)}")

        # Iterate over all living cells and check how many neighbors it has
        for cell_key in living_cells:
            # Acces the cell from the dictionary
            cell = living_cells[cell_key]
            # Check the cell
            check_cell(cell, cell_queue)
        living_cells = {}

        if verbose:
            print(f"Cell queue: {len(cell_queue)}")

        # Iterate over all cells in the cell queue and decides their fate
        # These are the only "intresting" cells, cells that are alive and
        # neighbors to these
        for cell in cell_queue:
            decicde_fate(cell, living_cells)
        # Reset the cell queue
        cell_queue = []
        if print_each_generation:
            print("#"*matrix_dim[0])
            io_helper.print_board(board, matrix_dim)

        generation += 1


    print(f"Final output | Generation: {generation} | Living Cells: {len(living_cells)}")
    if print_board:
        io_helper.print_board(board, matrix_dim)


    return living_cells, generation
